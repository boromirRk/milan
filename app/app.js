$(document).ready(function(){
    
$("#select").click(function(){
    $("#options").slideToggle();
});

$('.slick-slider').slick({
    dots: true,
    fade: true,
    responsive: true,
    adaptiveHeight: true,
    //autoplay: true,
    appendDots: $("#dots")
});
    
$('input').iCheck({
    checkboxClass: 'icheckbox_square-red',
    radioClass: 'iradio_square-red',
    increaseArea: '5%' // optional
});


d3.csv("resources/_IMPORTS/radnje.csv", function(data) {
    //console.log(data);
    
    for (var i in data){
        if (data[i].Drzava != undefined){
            domDataTable(data[i]);
        }
    }
    
});
    
    
});// document ready


var selectedParameters = [];
function searchData(e){
    var row, drzava, tip;
    var selected = capitalize(e.id);
    var table = document.getElementById("searchData").getElementsByTagName("tbody")[0];
    var tableRows = table.getElementsByTagName("tr");
    
    $("#"+e.id).toggleClass("selected");
    
    if (selectedParameters.length == 0){
        
        selectedParameters.push(selected);
        
    } else {
        var found = false;
        for (var i in selectedParameters){
            
            if (selectedParameters[i] == selected){ // if parametar is already in array then delete him
                var index = selectedParameters.indexOf(selectedParameters[i]);
                selectedParameters.splice(index, 1);
                found = true;
            } 
            
        }
        if (found == false){ // if parametar is not in array then push him in
            selectedParameters.push(selected);
        }
        
    }    
    //console.log(selectedParameters);

    var srbija, nemacka, fransiza, veleprodaja, maloprodaja;
    
    for (var p in selectedParameters){
        if (selectedParameters[p] == "Srbija"){
            srbija = true;
        } else if (selectedParameters[p] == "Nemacka"){
            nemacka = true;
        } else if (selectedParameters[p] == "Fransiza"){
            fransiza = true;
        } else if (selectedParameters[p] == "Veleprodaja"){
            veleprodaja = true;
        } else if (selectedParameters[p] == "Maloprodaja"){
            maloprodaja = true;
        }
    }
    
    if (srbija && nemacka){
        
        if (fransiza && veleprodaja && maloprodaja){

            for(var r in tableRows){

                tableRows[r].style.display = "";

            }    
            
        } else if (fransiza && veleprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava && tip == "Veleprodaja" || tip == "Fransiza"){
                    if (tip == "Veleprodaja" || tip == "Fransiza"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }    
            
        } else if (fransiza && maloprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    if (tip == "Maloprodaja" || tip == "Fransiza"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }
            
        } else if (veleprodaja && maloprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    if (tip == "Maloprodaja" || tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }
            
        } else if (fransiza){
            
            for(var r in tableRows){
                row = tableRows[r].children;

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
     
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    if (tip == "Fransiza"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }   
            
        } else if (veleprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    if (tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }      
            
        } else if (maloprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    if (tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            }
            
        } else {
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava || "Nemacka" == drzava){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }
        }
    
    } else if (srbija){ //************************************************************************************************ end srbija nemacka

        if (fransiza && veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Fransiza" || tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && veleprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Fransiza" || tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Fransiza" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Fransiza"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (veleprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 
   
        } else if (maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    if (tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else {

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Srbija" == drzava){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }
        }
        
    } else if (nemacka){ // ***************************************************************************************************** end srbija
        
        if (fransiza && veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Nemacka" == drzava){
                    if (tip == "Fransiza" || tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && veleprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Nemacka" == drzava){
                    if (tip == "Fransiza" || tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Nemacka" == drzava){
                    if (tip == "Fransiza" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Nemacka" == drzava){
                    if (tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza){
            
            for(var r in tableRows){
                row = tableRows[r].children;

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;

                if ("Nemacka" == drzava){
                    if (tip == "Fransiza"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                } else {
                    tableRows[r].style.display = "none";
                }

            }       
            
        } else if (veleprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;

                if ("Nemacka" == drzava){
                    if (tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                } else {
                    tableRows[r].style.display = "none";
                }

            }       
            
        } else if (maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;

                if ("Nemacka" == drzava){
                    if (tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                } else {
                    tableRows[r].style.display = "none";
                }

            }  

        } else {

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if ("Nemacka" == drzava){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }
        }
        
    } else { //******************************************************************************************************** end nemacka
      
        if (fransiza && veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Fransiza" || tip == "Veleprodaja" || tip == "Maloprodaja"){
                    if (tip == "Fransiza" || tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && veleprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Fransiza" || tip == "Veleprodaja"){
                    if (tip == "Fransiza" || tip == "Veleprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Fransiza" || tip == "Maloprodaja"){
                    if (tip == "Fransiza" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (veleprodaja && maloprodaja){

            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Veleprodaja" || tip == "Maloprodaja"){
                    if (tip == "Veleprodaja" || tip == "Maloprodaja"){
                        tableRows[r].style.display = "";
                    } else {
                        tableRows[r].style.display = "none";
                    }
                    
                } else {
                    tableRows[r].style.display = "none";
                }

            } 

        } else if (fransiza){
            
            for(var r in tableRows){
                row = tableRows[r].children;

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;

                if (tip == "Fransiza"){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }       
            
        } else if (veleprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Veleprodaja"){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }      
            
        } else if (maloprodaja){
            
            for(var r in tableRows){
                row = tableRows[r].children;//console.log(row);

                var drzava = row[0].innerHTML;
                var tip = row[1].innerHTML;
                 //console.log(word + " = " + drzava);
                if (tip == "Maloprodaja"){
                    tableRows[r].style.display = "";
                } else {
                    tableRows[r].style.display = "none";
                }

            }

        } else {
            for(var r in tableRows){
                tableRows[r].style.display = "";
            }
        }
    }
    
}



function domDataTable(data){
    $("#searchData tbody").append("<tr>"
                                    +"<td>"+data.Drzava+"</td>"
                                    +"<td>"+data.TipRadnje+"</td>"
                                    +"<td>"+data.BrojTelefona+"</td>"
                                    +"<td>"+data.Ulica+"</td>"+
                                  "</tr>"
                                 );
}

function capitalize(word){
    return word[0].toUpperCase() + word.slice(1);
}
